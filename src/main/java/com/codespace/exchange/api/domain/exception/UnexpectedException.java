package com.codespace.exchange.api.domain.exception;

import com.codespace.exchange.api.domain.model.CustomExceptionMessage;

public class UnexpectedException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7590263714432532441L;

	public UnexpectedException(CustomExceptionMessage customExceptionMessages) {
		super(customExceptionMessages);
	}

}

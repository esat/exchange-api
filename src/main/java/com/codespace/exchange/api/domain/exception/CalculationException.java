package com.codespace.exchange.api.domain.exception;

import com.codespace.exchange.api.domain.model.CustomExceptionMessage;

public class CalculationException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8893679529842104383L;

	public CalculationException(CustomExceptionMessage customExceptionMessage) {
		super(customExceptionMessage);
	}
	
}

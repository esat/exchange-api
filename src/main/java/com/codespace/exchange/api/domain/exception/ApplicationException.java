package com.codespace.exchange.api.domain.exception;

import com.codespace.exchange.api.domain.model.CustomExceptionMessage;

public class ApplicationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7190849128843914984L;
	
	private CustomExceptionMessage customExceptionMessages;
	
	public ApplicationException(CustomExceptionMessage customExceptionMessages) {
		this.customExceptionMessages = customExceptionMessages;
	}

	public CustomExceptionMessage getCustomExceptionMessages() {
		return customExceptionMessages;
	}

	public void setCustomExceptionMessages(CustomExceptionMessage customExceptionMessages) {
		this.customExceptionMessages = customExceptionMessages;
	}
}

package com.codespace.exchange.api.domain.exception;

import com.codespace.exchange.api.domain.model.CustomExceptionMessage;

public class InvalidBaseOrTargetCurrencyException extends RatesApiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2942260795109973782L;
	
	public InvalidBaseOrTargetCurrencyException(CustomExceptionMessage customExceptionMessage) {
		super(customExceptionMessage);
	}

}

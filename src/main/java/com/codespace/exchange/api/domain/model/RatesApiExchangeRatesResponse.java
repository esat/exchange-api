package com.codespace.exchange.api.domain.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public class RatesApiExchangeRatesResponse {
	private String base;
	private Date date;
	private Map<String, BigDecimal> rates;

	public RatesApiExchangeRatesResponse() {
	}

	public RatesApiExchangeRatesResponse(String base, Date date, Map<String, BigDecimal> rates) {
		this.base = base;
		this.date = date;
		this.rates = rates;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Map<String, BigDecimal> getRates() {
		return rates;
	}

	public void setRates(Map<String, BigDecimal> rates) {
		this.rates = rates;
	}

}

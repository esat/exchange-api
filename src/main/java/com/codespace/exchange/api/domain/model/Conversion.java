package com.codespace.exchange.api.domain.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity		
public class Conversion {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long transactionId;
	
	@Column(name="source_currency")
	private String sourceCurrency;
	
	@Column(name="source_amount")
	private BigDecimal sourceAmount;
	
	@Column(name="target_currency")
	private String targetCurrency;
	
	@Column(name="calculated_amount")
	private BigDecimal calculatedAmount;
	
	@Column(name="transaction_date")
	private Date transactionDate;
	
	public Conversion() {
	}

	public Conversion(Long id, String sourceCurrency, BigDecimal sourceAmount, String targetCurrency,
			BigDecimal calculatedAmount, Date transactionDate) {
		this.transactionId = id;
		this.sourceCurrency = sourceCurrency;
		this.sourceAmount = sourceAmount;
		this.targetCurrency = targetCurrency;
		this.calculatedAmount = calculatedAmount;
		this.transactionDate = transactionDate;
	}
		
	
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	public String getSourceCurrency() {
		return sourceCurrency;
	}
	public void setSourceCurrency(String sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}
	public BigDecimal getSourceAmount() {
		return sourceAmount;
	}
	public void setSourceAmount(BigDecimal sourceAmount) {
		this.sourceAmount = sourceAmount;
	}
	public String getTargetCurrency() {
		return targetCurrency;
	}
	public void setTargetCurrency(String targetCurrency) {
		this.targetCurrency = targetCurrency;
	}
	public BigDecimal getCalculatedAmount() {
		return calculatedAmount;
	}
	public void setCalculatedAmount(BigDecimal calculatedAmount) {
		this.calculatedAmount = calculatedAmount;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	@Override
	public String toString() {
		return "Conversion [transactionId=" + transactionId + ", sourceCurrency=" + sourceCurrency + ", sourceAmount="
				+ sourceAmount + ", targetCurrency=" + targetCurrency + ", calculatedAmount=" + calculatedAmount
				+ ", transactionDate=" + transactionDate + "]";
	}
	
}

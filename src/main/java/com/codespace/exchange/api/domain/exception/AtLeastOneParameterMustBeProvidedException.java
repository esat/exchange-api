package com.codespace.exchange.api.domain.exception;

import com.codespace.exchange.api.domain.model.CustomExceptionMessage;

public class AtLeastOneParameterMustBeProvidedException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 100897347303179799L;

	public AtLeastOneParameterMustBeProvidedException(CustomExceptionMessage customExceptionMessage) {
		super(customExceptionMessage);
	}
}

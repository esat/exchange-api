package com.codespace.exchange.api.domain.exception;

import com.codespace.exchange.api.domain.model.CustomExceptionMessage;

public class EntitySaveIsNotSuccessfulException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5966256433961384791L;
	
	public EntitySaveIsNotSuccessfulException(CustomExceptionMessage customExceptionMessage) {
		super(customExceptionMessage);
	}

}

package com.codespace.exchange.api.domain.model;

import java.util.Date;

import com.codespace.exchange.api.util.DateUtils;

public class ErrorDetail {
	private String errorCode;
	private String errorMsg;
	private Date date;
	
	public ErrorDetail(CustomExceptionMessage customExceptionMessage) {
		this.date = DateUtils.now();
		this.errorCode = customExceptionMessage.name();
		this.errorMsg = customExceptionMessage.getErrorMsg();
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}

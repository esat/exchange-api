package com.codespace.exchange.api.domain.model;

import java.math.BigDecimal;
import java.util.Date;

public class ExchangeRateResult {
	private String fromCurrency;
	private String toCurrency;
	private BigDecimal exchangeRate;
	private Date date;
	
	public ExchangeRateResult() {
	}

	public ExchangeRateResult(String fromCurrency, String toCurrency, BigDecimal exchangeRate, Date date) {
		this.fromCurrency = fromCurrency;
		this.toCurrency = toCurrency;
		this.exchangeRate = exchangeRate;
		this.date = date;
	}
	public String getFromCurrency() {
		return fromCurrency;
	}
	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}
	public String getToCurrency() {
		return toCurrency;
	}
	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ExchangeRateResponse [fromCurrency=" + fromCurrency + ", toCurrency=" + toCurrency + ", exchangeRate="
				+ exchangeRate + ", date=" + date + "]";
	}

}

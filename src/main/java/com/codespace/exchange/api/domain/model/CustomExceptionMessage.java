package com.codespace.exchange.api.domain.model;

public enum CustomExceptionMessage {
	ERR1("There is an unexpected error while geting exchange rates from service provider!"),
	ERR2("There is an validation error at input currencies. Please check!"),
	ERR3("There comes an unexpected exchange rate result from provider. Please check your input currencies!"),
	ERR4("There is an unexpected error while making calculation to target currency!"),
	ERR5("There is an unexpected error while saving conversion transaction!"),
	ERR6("At leat one of transaction id or transacton date parameters must be provided!"),
	ERR7("Empty paramaters are not allowed. Please check your inputs!"),
	ERR8("There is an unexpected error!");


	private final String errorMsg;

	CustomExceptionMessage(final String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg() {
		return errorMsg;
	}
	
}

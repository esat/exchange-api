package com.codespace.exchange.api.domain.exception;

import com.codespace.exchange.api.domain.model.CustomExceptionMessage;

public class RatesApiException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4794073139717520968L;
	
	public RatesApiException(CustomExceptionMessage customExceptionMessage) {
		super(customExceptionMessage);
	}

}

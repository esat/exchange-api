package com.codespace.exchange.api.domain.exception;

import com.codespace.exchange.api.domain.model.CustomExceptionMessage;

public class UnexpectedExchangeRatesResultException extends RatesApiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1732193580846079189L;
	
	public UnexpectedExchangeRatesResultException(CustomExceptionMessage customExceptionMessage) {
		super(customExceptionMessage);
	}

}

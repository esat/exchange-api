package com.codespace.exchange.api.domain.exception;

import com.codespace.exchange.api.domain.model.CustomExceptionMessage;

public class UnsuccessfulExchangeRateRequestException extends RatesApiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5843520637763619063L;
	
	public UnsuccessfulExchangeRateRequestException(CustomExceptionMessage customExceptionMessage) {
		super(customExceptionMessage);
	}

}

package com.codespace.exchange.api.service;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.codespace.exchange.api.client.CurrencyServiceProviderClient;
import com.codespace.exchange.api.domain.exception.ApplicationException;
import com.codespace.exchange.api.domain.exception.UnexpectedExchangeRatesResultException;
import com.codespace.exchange.api.domain.model.CustomExceptionMessage;
import com.codespace.exchange.api.domain.model.ExchangeRateResult;
import com.codespace.exchange.api.domain.model.RatesApiExchangeRatesResponse;
import com.codespace.exchange.api.util.DateUtils;

@Service
public class ExchangeRateService {

	@Autowired
	CurrencyServiceProviderClient currencyServiceProviderClient;
	
	public ExchangeRateResult getLatestExchangeRate(String fromCurrency, String toCurrency) throws ApplicationException {
		RatesApiExchangeRatesResponse ratesApiExchangeRatesResponse = currencyServiceProviderClient.getLatestExchangeRate(fromCurrency, toCurrency);		
		ExchangeRateResult exchangeRateResponse = convertResponse(ratesApiExchangeRatesResponse);

		return exchangeRateResponse;
	}

	private ExchangeRateResult convertResponse(RatesApiExchangeRatesResponse ratesApiExchangeRatesResponse) throws ApplicationException {
		String toCurrency = null;
		BigDecimal exchangeRate = null;
		
		Map<String, BigDecimal> toExchangeRates = ratesApiExchangeRatesResponse.getRates();
		if(CollectionUtils.isEmpty(toExchangeRates)) {
			throw new UnexpectedExchangeRatesResultException(CustomExceptionMessage.ERR3);
		}

		Map.Entry<String,BigDecimal> entry = toExchangeRates.entrySet().iterator().next();
		toCurrency = entry.getKey();
		exchangeRate = entry.getValue();
		
		String fromCurrency = ratesApiExchangeRatesResponse.getBase();

		ExchangeRateResult exchangeRateResponse = new ExchangeRateResult();
		exchangeRateResponse.setDate(DateUtils.now());
		exchangeRateResponse.setFromCurrency(fromCurrency);
		exchangeRateResponse.setToCurrency(toCurrency);
		exchangeRateResponse.setExchangeRate(exchangeRate);

		return exchangeRateResponse;
	}

}

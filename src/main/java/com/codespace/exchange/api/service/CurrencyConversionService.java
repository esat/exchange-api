package com.codespace.exchange.api.service;

import static com.codespace.exchange.api.util.DateUtils.now;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.codespace.exchange.api.domain.exception.ApplicationException;
import com.codespace.exchange.api.domain.exception.AtLeastOneParameterMustBeProvidedException;
import com.codespace.exchange.api.domain.exception.CalculationException;
import com.codespace.exchange.api.domain.exception.EntitySaveIsNotSuccessfulException;
import com.codespace.exchange.api.domain.exception.UnexpectedException;
import com.codespace.exchange.api.domain.model.Conversion;
import com.codespace.exchange.api.domain.model.CustomExceptionMessage;
import com.codespace.exchange.api.domain.model.ExchangeRateResult;
import com.codespace.exchange.api.repository.ConversionRepository;

@Service
public class CurrencyConversionService {

	@Autowired
	ConversionRepository conversionRepository;

	@Autowired
	ExchangeRateService exchangeRateService;

	public Page<Conversion> findByParams(Long transactionId, Date transactionDate, Pageable pageable)
			throws ApplicationException {

		if (Objects.isNull(transactionId) && Objects.isNull(transactionDate)) {
			throw new AtLeastOneParameterMustBeProvidedException(CustomExceptionMessage.ERR6);
		}

		Page<Conversion> conversions = null;
		try {
			if (Objects.nonNull(transactionDate) && Objects.nonNull(transactionId)) {
				conversions = conversionRepository.findByTransactionIdAndTransactionDate(transactionId, transactionDate,
						pageable);
			} else {
				conversions = conversionRepository.findByTransactionIdOrTransactionDate(transactionId, transactionDate,
						pageable);
			}

		} catch (Exception e) {
			throw new UnexpectedException(CustomExceptionMessage.ERR8);
		}

		return conversions;
	}

	public Conversion convert(String amount, String sourceCurrency, String targetCurrency) throws ApplicationException {
		Conversion conversion = null;

		try {
			BigDecimal formattedSourceAmount = new BigDecimal(amount);
			BigDecimal calculatedAmount = calculateInTargetCurrency(formattedSourceAmount, sourceCurrency,
					targetCurrency);
			conversion = saveConversion(formattedSourceAmount, sourceCurrency, targetCurrency, calculatedAmount);
		} 
		catch(ApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			throw new UnexpectedException(CustomExceptionMessage.ERR8);
		}

		return conversion;
	}

	public BigDecimal calculateInTargetCurrency(BigDecimal amount, String sourceCurrency, String targetCurrency)
			throws ApplicationException {
		ExchangeRateResult exchangeRateResult = exchangeRateService.getLatestExchangeRate(sourceCurrency,
				targetCurrency);

		BigDecimal calculatedAmount = null;
		try {
			BigDecimal exchangeRate = exchangeRateResult.getExchangeRate();
			calculatedAmount = amount.multiply(exchangeRate);
		} catch (Exception e) {
			throw new CalculationException(CustomExceptionMessage.ERR4);
		}

		return calculatedAmount;
	}

	public Conversion saveConversion(BigDecimal sourceAmount, String sourceCurrency, String targetCurrency,
			BigDecimal calculatedAmount) throws ApplicationException {
		Conversion conversion = new Conversion();
		conversion.setSourceAmount(sourceAmount);
		conversion.setSourceCurrency(sourceCurrency);
		conversion.setTargetCurrency(targetCurrency);
		conversion.setCalculatedAmount(calculatedAmount);
		conversion.setTransactionDate(now());

		try {
			conversion = conversionRepository.save(conversion);
		} catch (Exception e) {
			throw new EntitySaveIsNotSuccessfulException(CustomExceptionMessage.ERR5);
		}

		return conversion;
	}

}

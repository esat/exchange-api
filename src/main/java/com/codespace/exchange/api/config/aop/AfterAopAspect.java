package com.codespace.exchange.api.config.aop;

import static com.codespace.exchange.api.util.GenericUtils.getGenericExceptionLog;
import static com.codespace.exchange.api.util.GenericUtils.getGenericSuccessLog;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class AfterAopAspect {

	Logger logger = LoggerFactory.getLogger(AfterAopAspect.class);

	@AfterReturning(value = "com.codespace.exchange.api.config.aop.CommonJoinPointConfig.dataLayerExecution()", returning = "result")
	public void afterFromDataLayer(JoinPoint joinPoint, Object result) {
		String logMsg = getGenericSuccessLog(joinPoint, result);
		logger.info(logMsg);
	}

	@AfterThrowing(value = "com.codespace.exchange.api.config.aop.CommonJoinPointConfig.dataLayerExecution()", throwing = "e")
	public void afterThrowingFromDataLayer(JoinPoint joinPoint, Throwable e) {
		String logMsg = getGenericExceptionLog(joinPoint, e);
		logger.error(logMsg, e);
	}

	@AfterReturning(value = "com.codespace.exchange.api.config.aop.CommonJoinPointConfig.businessLayerExecution()", returning = "result")
	public void afterFromBusinessLayer(JoinPoint joinPoint, Object result) {
		String logMsg = getGenericSuccessLog(joinPoint, result);
		logger.info(logMsg);
	}

	@AfterThrowing(value = "com.codespace.exchange.api.config.aop.CommonJoinPointConfig.businessLayerExecution()", throwing = "e")
	public void afterThrowingFromBusinessLayer(JoinPoint joinPoint, Throwable e) {
		String logMsg = getGenericExceptionLog(joinPoint, e);
		logger.error(logMsg, e);
	}

	@AfterReturning(value = "com.codespace.exchange.api.config.aop.CommonJoinPointConfig.webLayerExecution()", returning = "result")
	public void afterFromWebLayerExecution(JoinPoint joinPoint, Object result) {
		String logMsg = getGenericSuccessLog(joinPoint, result);
		logger.info(logMsg);
	}

	@AfterThrowing(value = "com.codespace.exchange.api.config.aop.CommonJoinPointConfig.webLayerExecution()", throwing = "e")
	public void afterThrowingFromWebLayerExecution(JoinPoint joinPoint, Throwable e) {
		String logMsg = getGenericExceptionLog(joinPoint, e);
		logger.error(logMsg, e);
	}
}

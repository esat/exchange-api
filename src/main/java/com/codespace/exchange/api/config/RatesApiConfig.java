package com.codespace.exchange.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RatesApiConfig {
	
	@Value("${rates.api.exchange.service.url}")
	private String ratesApiExchangeServiceUrl;

	public String getRatesApiExchangeServiceUrl() {
		return ratesApiExchangeServiceUrl;
	}

	public void setRatesApiExchangeServiceUrl(String ratesApiExchangeServiceUrl) {
		this.ratesApiExchangeServiceUrl = ratesApiExchangeServiceUrl;
	}
	
}

package com.codespace.exchange.api.config.aop;

import org.aspectj.lang.annotation.Pointcut;

public class CommonJoinPointConfig {
	@Pointcut("execution(* com.codespace.exchange.api.repository.*.*(..))")
	public void dataLayerExecution(){}
	
	@Pointcut("execution(* com.codespace.exchange.api.service.*.*(..))")
	public void businessLayerExecution(){}
	
	@Pointcut("execution(* com.codespace.exchange.api.controller.*.*(..))")
	public void webLayerExecution(){}
}

package com.codespace.exchange.api.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.codespace.exchange.api.domain.model.Conversion;

@Repository
public interface ConversionRepository extends PagingAndSortingRepository<Conversion, Long> {
	@Query("select c from Conversion c where c.id = :transactionId and TRUNC(c.transactionDate) = :transactionDate")
	Page<Conversion> findByTransactionIdAndTransactionDate(@Param("transactionId") Long transactionId, @Param("transactionDate") Date transactionDate, Pageable pageable);
	
	@Query("select c from Conversion c where c.id = :transactionId or TRUNC(c.transactionDate) = :transactionDate")
	Page<Conversion> findByTransactionIdOrTransactionDate(@Param("transactionId") Long transactionId, @Param("transactionDate") Date transactionDate, Pageable pageable);
}

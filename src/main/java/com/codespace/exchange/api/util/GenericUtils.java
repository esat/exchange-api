package com.codespace.exchange.api.util;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;

public class GenericUtils {
	
	private static final String LINE_SEPARATOR  = System.getProperty("line.separator");
	private static final String DOT_STR = ".";
	
	public static String getGenericSuccessLog(JoinPoint joinPoint, Object result) {
		Signature signature = joinPoint.getSignature();
		String packageName = signature.getDeclaringTypeName();
		String methodName = signature.getName();
		String arguments = Arrays.toString(joinPoint.getArgs());
		String logMsg = "Method: " + packageName + "." + methodName + " with arguments: " +
		arguments + " is succesfully returned: " + LINE_SEPARATOR + result;
		
		return logMsg;
	}
	
	public static String getGenericExceptionLog(JoinPoint joinPoint, Throwable e) {
		Signature signature = joinPoint.getSignature();
		String packageName = signature.getDeclaringTypeName();
		String methodName = signature.getName();
		String arguments = Arrays.toString(joinPoint.getArgs());
		String logMsg = "There occurs an " + e.getClass().getName() + LINE_SEPARATOR
				+ "at method: " + packageName + DOT_STR + methodName + " with arguments: "
				+ arguments + LINE_SEPARATOR;

		return logMsg;
	}

}

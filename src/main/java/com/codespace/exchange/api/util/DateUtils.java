package com.codespace.exchange.api.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	public static final String ISO8601_DATE_FORMAT = "yyyy-MM-dd";
	
	public static Date now() {
		return Calendar.getInstance().getTime();
	}
	
}

package com.codespace.exchange.api.client;

import static com.codespace.exchange.api.util.SerializationUtils.gson;

import java.util.Arrays;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.codespace.exchange.api.config.RatesApiConfig;
import com.codespace.exchange.api.domain.exception.InvalidBaseOrTargetCurrencyException;
import com.codespace.exchange.api.domain.exception.RatesApiException;
import com.codespace.exchange.api.domain.exception.UnsuccessfulExchangeRateRequestException;
import com.codespace.exchange.api.domain.model.CustomExceptionMessage;
import com.codespace.exchange.api.domain.model.RatesApiExchangeRatesResponse;

@Service
public class CurrencyServiceProviderClient {

	@Autowired
	@Qualifier("ratesApiRestTemplate")
	RestTemplate restTemplate;

	@Autowired
	RatesApiConfig ratesApiConfig;

	public RatesApiExchangeRatesResponse getLatestExchangeRate(String baseCurrency, String toCurrency)
			throws RatesApiException {
		RatesApiExchangeRatesResponse ratesApiExchangeResponse = null;

		try {
			String latestExchangeRatesUrl = ratesApiConfig.getRatesApiExchangeServiceUrl();
			latestExchangeRatesUrl = String.format(latestExchangeRatesUrl, baseCurrency, toCurrency);

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

			HttpEntity<String> request = new HttpEntity<String>(headers);
			ResponseEntity<RatesApiExchangeRatesResponse> responseEntity = restTemplate.exchange(latestExchangeRatesUrl, HttpMethod.GET, request, RatesApiExchangeRatesResponse.class);

			if (Objects.isNull(responseEntity)) {
				throw new UnsuccessfulExchangeRateRequestException(CustomExceptionMessage.ERR1);
			}

			HttpStatus status = responseEntity.getStatusCode();
			if (Objects.equals(status, HttpStatus.BAD_REQUEST)) {
				throw new InvalidBaseOrTargetCurrencyException(CustomExceptionMessage.ERR2);
			}

			if (!Objects.equals(status, HttpStatus.OK)) {
				throw new UnsuccessfulExchangeRateRequestException(CustomExceptionMessage.ERR1);
			}

			ratesApiExchangeResponse = responseEntity.getBody();
		} catch (RatesApiException e) {
			throw e;
		} catch (RestClientException e) {
			throw new InvalidBaseOrTargetCurrencyException(CustomExceptionMessage.ERR2);
		} catch (Exception e) {
			throw new UnsuccessfulExchangeRateRequestException(CustomExceptionMessage.ERR1);
		}

		return ratesApiExchangeResponse;
	}
}

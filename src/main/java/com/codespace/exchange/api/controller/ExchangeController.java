package com.codespace.exchange.api.controller;

import static com.codespace.exchange.api.util.DateUtils.ISO8601_DATE_FORMAT;

import java.util.Date;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codespace.exchange.api.domain.exception.ApplicationException;
import com.codespace.exchange.api.domain.model.Conversion;
import com.codespace.exchange.api.domain.model.ExchangeRateResult;
import com.codespace.exchange.api.service.CurrencyConversionService;
import com.codespace.exchange.api.service.ExchangeRateService;

@RestController
@Validated
public class ExchangeController {

	@Autowired
	ExchangeRateService exchangeRateService;

	@Autowired
	CurrencyConversionService conversionService;
	
	@GetMapping("/exchangeRate")
	public ExchangeRateResult getExchangeRate(@RequestParam("from") @Size(min=1) String fromCurrency ,
			@RequestParam("to") @Size(min=1) String toCurrency) throws ApplicationException {
		
		ExchangeRateResult exchangeRateResponse = exchangeRateService.getLatestExchangeRate(fromCurrency, toCurrency);
		return exchangeRateResponse;
	}

	@GetMapping("/convert")
	public Conversion convert(@RequestParam("amount") @Size(min=1) String amount, @RequestParam("from") @Size(min=1) String fromCurrency,
			@RequestParam("to") @Size(min=1) String toCurrency) throws ApplicationException {
		
		Conversion conversion = conversionService.convert(amount, fromCurrency, toCurrency);
		return conversion;
	}

	@GetMapping("/conversions")
	public Page<Conversion> filterConversions(@RequestParam(value = "transactionId", required = false) Long transactionId,
			@RequestParam(value = "transactionDate", required = false) @DateTimeFormat(pattern = ISO8601_DATE_FORMAT) Date transactionDate,
			Pageable pageable) throws ApplicationException {

		Page<Conversion> conversions = null;
		conversions = conversionService.findByParams(transactionId, transactionDate, pageable);

		return conversions;
	}
	
}

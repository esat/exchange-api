package com.codespace.exchange.api.controller.advice;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.codespace.exchange.api.domain.exception.ApplicationException;
import com.codespace.exchange.api.domain.exception.AtLeastOneParameterMustBeProvidedException;
import com.codespace.exchange.api.domain.exception.CalculationException;
import com.codespace.exchange.api.domain.exception.EntitySaveIsNotSuccessfulException;
import com.codespace.exchange.api.domain.exception.InvalidBaseOrTargetCurrencyException;
import com.codespace.exchange.api.domain.exception.UnexpectedExchangeRatesResultException;
import com.codespace.exchange.api.domain.exception.UnsuccessfulExchangeRateRequestException;
import com.codespace.exchange.api.domain.model.CustomExceptionMessage;
import com.codespace.exchange.api.domain.model.ErrorDetail;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = UnsuccessfulExchangeRateRequestException.class)
	protected ResponseEntity<ErrorDetail> handleUnsuccessfulExchangeRateRequestException(ApplicationException exception,
			WebRequest request) {
		ErrorDetail errorDetail = new ErrorDetail(exception.getCustomExceptionMessages());
		return new ResponseEntity<>(errorDetail, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = InvalidBaseOrTargetCurrencyException.class)
	protected ResponseEntity<Object> handleInvalidBaseOrTargetCurrencyException(ApplicationException exception,
			WebRequest request) {
		ErrorDetail errorDetail = new ErrorDetail(exception.getCustomExceptionMessages());
		return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = UnexpectedExchangeRatesResultException.class)
	protected ResponseEntity<Object> handleUnexpectedExchangeRatesResultException(ApplicationException exception,
			WebRequest request) {
		ErrorDetail errorDetail = new ErrorDetail(exception.getCustomExceptionMessages());
		return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = CalculationException.class)
	protected ResponseEntity<Object> handleCalculationException(ApplicationException exception, WebRequest request) {
		ErrorDetail errorDetail = new ErrorDetail(exception.getCustomExceptionMessages());
		return new ResponseEntity<>(errorDetail, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = EntitySaveIsNotSuccessfulException.class)
	protected ResponseEntity<Object> handleEntitySaveIsNotSuccessfulException(ApplicationException exception,
			WebRequest request) {
		ErrorDetail errorDetail = new ErrorDetail(exception.getCustomExceptionMessages());
		return new ResponseEntity<>(errorDetail, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = AtLeastOneParameterMustBeProvidedException.class)
	protected ResponseEntity<Object> handleAtLeastOneParameterMustBeProvidedException(ApplicationException exception,
			WebRequest request) {
		ErrorDetail errorDetail = new ErrorDetail(exception.getCustomExceptionMessages());
		return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value=ConstraintViolationException.class)
	protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException exception) {
		ErrorDetail errorDetail = new ErrorDetail(CustomExceptionMessage.ERR7);
		return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
	}
}

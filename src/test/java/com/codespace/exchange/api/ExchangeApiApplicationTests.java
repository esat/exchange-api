package com.codespace.exchange.api;

import static com.codespace.exchange.api.util.DateUtils.now;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.codespace.exchange.api.client.CurrencyServiceProviderClient;
import com.codespace.exchange.api.domain.exception.ApplicationException;
import com.codespace.exchange.api.domain.exception.AtLeastOneParameterMustBeProvidedException;
import com.codespace.exchange.api.domain.exception.CalculationException;
import com.codespace.exchange.api.domain.exception.EntitySaveIsNotSuccessfulException;
import com.codespace.exchange.api.domain.exception.UnexpectedException;
import com.codespace.exchange.api.domain.exception.UnexpectedExchangeRatesResultException;
import com.codespace.exchange.api.domain.model.ExchangeRateResult;
import com.codespace.exchange.api.domain.model.RatesApiExchangeRatesResponse;
import com.codespace.exchange.api.repository.ConversionRepository;
import com.codespace.exchange.api.service.CurrencyConversionService;
import com.codespace.exchange.api.service.ExchangeRateService;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeApiApplicationTests {
	
	@InjectMocks
	ExchangeRateService exchangeRateService;
	
	@InjectMocks
	CurrencyConversionService currencyConversionService;
	
	@Mock
	CurrencyServiceProviderClient currencyServiceProviderClient;
	
	@Mock
	ConversionRepository conversionRepository;
	
	@Mock
	ExchangeRateService exchangeRateServiceMock;
	
	private static RatesApiExchangeRatesResponse ratesApiExchangeRatesResponse;
	private static RatesApiExchangeRatesResponse ratesApiExchangeRatesResponseWithEmptyResultingRates;
	
	private static Long sampleLong;
	private static Date now;
	
	private static ExchangeRateResult exchangeRateResult;
	
	private static BigDecimal sampleAmount;
	
	@BeforeClass
	public static void beforeClass() {
		Map<String, BigDecimal> resultingRates = new HashMap<String, BigDecimal>();
		resultingRates.put("EUR", new BigDecimal(1.15));
		
		ratesApiExchangeRatesResponse = new RatesApiExchangeRatesResponse("USD", now(), resultingRates);
		ratesApiExchangeRatesResponseWithEmptyResultingRates = new RatesApiExchangeRatesResponse("EUR", now(), null);
	
		sampleLong = new Long(1);
		now = now();
		
		exchangeRateResult = new ExchangeRateResult("TRY", "GBP", new BigDecimal(6.91), now);

		sampleAmount = new BigDecimal(10);
	}
	
	@Test
	public void testGetLatestExchangeRate() throws ApplicationException {
		when(currencyServiceProviderClient.getLatestExchangeRate(anyString(), anyString())).thenReturn(ratesApiExchangeRatesResponse);
		ExchangeRateResult exchangeRateResult = exchangeRateService.getLatestExchangeRate(anyString(), anyString());
		
		Map.Entry<String,BigDecimal> entry = ratesApiExchangeRatesResponse.getRates().entrySet().iterator().next();
		String resultingToCurrency = entry.getKey();
		BigDecimal resultingRate = entry.getValue();
		
		assertEquals(exchangeRateResult.getFromCurrency(), "USD");	
		assertEquals(resultingToCurrency, "EUR");	
		assertEquals(exchangeRateResult.getExchangeRate(), resultingRate);
	}
	
	@Test(expected=UnexpectedExchangeRatesResultException.class)
	public void testGetLatestExchangeRateThrowing() throws ApplicationException {
		when(currencyServiceProviderClient.getLatestExchangeRate(anyString(), anyString())).thenReturn(ratesApiExchangeRatesResponseWithEmptyResultingRates);
		exchangeRateService.getLatestExchangeRate(anyString(), anyString());
	}
	
	@Test
	public void testForFirstRespostioryMethodAtFindByParams() throws ApplicationException{
		currencyConversionService.findByParams(sampleLong, now, null);
		
		Mockito.verify(conversionRepository, Mockito.times(1)).findByTransactionIdAndTransactionDate(sampleLong, now, null);
	}
	
	@Test
	public void testForSecondRespostioryMethodAtFindByParams() throws ApplicationException{
		currencyConversionService.findByParams(null, now, null);
		
		Mockito.verify(conversionRepository, Mockito.times(1)).findByTransactionIdOrTransactionDate(null, now, null);
	}
	
	@Test(expected=AtLeastOneParameterMustBeProvidedException.class)
	public void testFindByParamsThrowingAtLeastOneParameterMustBeProvidedException() throws ApplicationException{
		currencyConversionService.findByParams(null, null, null);
	}
	
	@Test(expected=EntitySaveIsNotSuccessfulException.class)
	public void testSaveConversionThrowingEntitySaveIsNotSuccessfulException() throws ApplicationException{
		when(conversionRepository.save(any())).thenThrow(new PersistenceException());
		
		currencyConversionService.saveConversion(null, null, null, null);
	}
	
	@Test
	public void testCalculateInTargetCurrency() throws ApplicationException{
		when(exchangeRateServiceMock.getLatestExchangeRate(anyString(), anyString())).thenReturn(exchangeRateResult);

		BigDecimal calculatedAmount = currencyConversionService.calculateInTargetCurrency(sampleAmount, "USD", "TRY");
		
		BigDecimal rate = exchangeRateResult.getExchangeRate();
		BigDecimal expectedAmount = sampleAmount.multiply(rate);
		
		assertEquals(calculatedAmount, expectedAmount);	
	}
	
	@Test(expected=CalculationException.class)
	public void testCalculateInTargetCurrencyThrowingEntitySaveIsNotSuccessfulException() throws ApplicationException{
		when(exchangeRateServiceMock.getLatestExchangeRate(anyString(), anyString())).thenReturn(exchangeRateResult);
		
		currencyConversionService.calculateInTargetCurrency(null, "USD", "TRY");
	}
	
	@Test(expected=UnexpectedException.class)
	public void testConvertThrowinUnexpectedException() throws ApplicationException{
		currencyConversionService.convert(null, "USD", "TRY");
	}
	
}


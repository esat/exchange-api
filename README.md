# exchange-api

to get exchange rate

GET localhost:8080/exchangeRate?from=fromCurrency&to=toCurrency

to convert some amount of currency to other

GET localhost:8080/convert?amount=baseAmount&from=fromCurrency&to=toCurrency

to get saved conversion transactions by params 

GET localhost:8080/conversions?transactionId=transactionId&transactionDate=dateInISO8601&size=sizeNumber&page=pageNumber

api documentation can be reached from 

http://localhost:8080/swagger-ui.html

for h2 db console

http://localhost:8080/h2-console/ 
(set jdbc:h2:mem:testdb as jdbc url if it is not)

https://ratesapi.io/ have been used as currency service provider for unlimited request

full application is also deployed and on working at heroku cloud and it can be reached from

https://currency-exchange-api.herokuapp.com







